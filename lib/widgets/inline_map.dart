import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_web/flutter_native_web.dart';

class InlineMapWidget extends StatefulWidget {
  final String lat;
  final String long;
  final String colorCode;
  final String mapName;

  const InlineMapWidget(
      {Key key,
      @required this.mapName,
      @required this.lat,
      @required this.long,
      @required this.colorCode})
      : super(key: key);
  @override
  _InlineMapWidgetState createState() => _InlineMapWidgetState();
}

class _InlineMapWidgetState extends State<InlineMapWidget> {
  WebController webController;
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    FlutterNativeWeb flutterWebView = new FlutterNativeWeb(
      onWebCreated: onWebCreated
    );
    return Container(
      child: new Column(
        children: <Widget>[
          new Container(
            child: flutterWebView,
            height: 600.0,
            width: 500.0,
          ),
        ],
      ),
    );
  }

  void onWebCreated(webController) {
    final _lat = widget.lat;
    final _long = widget.long;
    final _color = widget.colorCode;
    this.webController = webController;
    print(widget.colorCode + "  HEll");
    print("TEST  " + widget.mapName);
    this.webController.loadUrl(widget.mapName != null || widget.mapName != "" 
        ? "https://gisapi.mea.or.th/embed/landmark?q=${widget.mapName}&pinColor=${widget.colorCode}&z=17&mt=standard&s=true&key=d3259813dbd6b46d43a957ec620ef5d4&latlonOnly=false"
        : "https://gisapi.mea.or.th/embed/landmark?q=$_lat,$_long&pinColor=$_color&z=17&mt=standard&s=true&key=d3259813dbd6b46d43a957ec620ef5d4&${'latlonOnly=true'}");
    this.webController.onPageStarted.listen((url) => print("Loading $url"));
    this.webController.onPageFinished.listen((url) {
      // setState(() {
      //   this._isLoading = false;
      // });
      print("Finished loading $url");
    });
  }
}
// https://gisapi.mea.or.th/embed/landmark?q=LTRAT3200002563242&pinColor=217cc7&z=17&mt=standard&s=true&key=6dc11b4a1498c2762452aca43c9186fb&latlonOnly=false