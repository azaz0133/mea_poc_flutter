import 'dart:io';

import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mea_smart_project_management/apis/firebase_database_api.dart';
import 'package:mea_smart_project_management/containers/pdf_view.dart';
import 'package:mea_smart_project_management/models/fb_file_model.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:flutter_full_pdf_viewer/full_pdf_viewer_scaffold.dart';
import 'package:path_provider/path_provider.dart';

class FileListWidget extends StatefulWidget {
  final String id;

  const FileListWidget({Key key, @required this.id}) : super(key: key);
  @override
  _FileListWidgetState createState() => _FileListWidgetState();
}

class _FileListWidgetState extends State<FileListWidget> {
  bool _anchorToBottom = false;
  FirebaseDatabaseUtil databaseUtil;

  @override
  void initState() {
    super.initState();
    databaseUtil = new FirebaseDatabaseUtil();
    databaseUtil.initState();
  }

  @override
  void dispose() {
    super.dispose();
    databaseUtil.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new FirebaseAnimatedList(
        key: new ValueKey<bool>(_anchorToBottom),
        query: databaseUtil.getProjectFiles(widget.id),
        reverse: _anchorToBottom,
        sort: _anchorToBottom
            ? (DataSnapshot a, DataSnapshot b) => b.key.compareTo(a.key)
            : null,
        itemBuilder: (
          BuildContext context,
          DataSnapshot snapshot,
          Animation<double> animation,
          int index,
        ) {
          return new SizeTransition(
            sizeFactor: animation,
            child: buildFileList(snapshot),
          );
        });
  }

  Future<File> createFileOfPdfUrl(String url) async {
    final filename = url.substring(url.lastIndexOf("/") + 1);
    var request = await HttpClient().getUrl(Uri.parse(url));
    var response = await request.close();
    var bytes = await consolidateHttpClientResponseBytes(response);
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = new File('$dir/$filename');
    await file.writeAsBytes(bytes);
    return file;
  }

  Widget buildFileList(DataSnapshot res) {
    FBFileProject file = FBFileProject.fromSnapshot(res);

    return InkWell(
      onTap: () {
        var p = file.file_name[file.file_name.length - 3] +
            file.file_name[file.file_name.length - 2] +
            file.file_name[file.file_name.length - 1];
        print("[ PDF ] " + p);
        if (p == "pdf") {
          if (Platform.isAndroid) {
            createFileOfPdfUrl(file.file_url).then((f) {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => PDFScreen(f.path)));
            });
          } else {
            _launchURL(file.file_url);
          }
        } else {
          _launchURL(file.file_url);
        }
      },
      child: new Card(
        child: ListTile(
          leading: Icon(Icons.file_download),
          title: Text(
            file.file_name,
            style: TextStyle(color: Colors.blueAccent),
          ),
        ),
      ),
    );
  }

  _launchURL(fileUrl) async {
    var url = fileUrl;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
